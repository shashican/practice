FROM ruby:2.5.1-slim-stretch

# installs should happen before app code is coped, to help build cache.
RUN apt-get update &&\
	apt-get install -y build-essential patch ruby-dev zlib1g-dev liblzma-dev libpq-dev 


# to address error: You must use Bundler 2 or greater with this lockfile.  
ENV BUNDLER_VERSION=2.1.4
RUN gem install bundler -v 2.1.4 

RUN mkdir /application
WORKDIR /application
COPY Gemfile .
COPY Gemfile.lock .  

RUN bundle install

# Copy application code
# Change to the application's directory 
# if the app code changes, docker will stop using cache from here on. Hence installs should happen before this.
COPY . /application
