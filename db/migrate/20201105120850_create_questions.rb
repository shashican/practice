class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|

      t.string :statement
      t.string :ans1
      t.string :ans2
      t.string :ans3
      t.string :ans4
      t.string :correct_ans

      t.timestamps
    end
  end
end
