class Question < ApplicationRecord
  validates_presence_of :statement, :ans1, :ans2, :ans3, :ans4, :correct_ans
end
