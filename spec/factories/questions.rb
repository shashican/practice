FactoryBot.define do
  factory :question do
    statement { Faker::Lorem.sentence }
    ans1 { Faker::Lorem.word }
    ans2 { Faker::Lorem.word }
    ans3 { Faker::Lorem.word }
    ans4 { Faker::Lorem.word }
    correct_ans { Faker::Lorem.word }
  end
end
