require 'rails_helper'

# Test suite for the Todo model
RSpec.describe Question, type: :model do
  # Validation tests
  it { should validate_presence_of(:statement) }
  it { should validate_presence_of(:ans1) }
  it { should validate_presence_of(:ans2) }
  it { should validate_presence_of(:ans3) }
  it { should validate_presence_of(:ans4) }
  it { should validate_presence_of(:correct_ans) }
end
