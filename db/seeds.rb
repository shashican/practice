# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
group :development do
  # development specific seeding code
  10.times do
    Question.create(
      statement: Faker::Lorem.question(word_count: 18),
      ans1: Faker::Quotes::Rajnikanth.joke,
      ans2: Faker::Quotes::Rajnikanth.joke,
      ans3: Faker::Quotes::Rajnikanth.joke,
      ans4: Faker::Quotes::Rajnikanth.joke,
      correct_ans: Faker::Quotes::Rajnikanth.joke
    )
  end
end
